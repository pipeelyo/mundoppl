/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ['images.ctfassets.net','placeholder', 'via.placeholder.com',],
  },
}

module.exports = nextConfig
